# Signal Device

A display that shows signal waves and produces sounds. Can be used as a prop (for example in a 3D puzzle).

You can try it out at the [itch.io page](https://karakuja.itch.io/signal-device).

