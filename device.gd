extends Node3D

var shaderMaterial
var knob1Active = false
var knob2Active = false
var knob3Active = false

var phase1 = 0
var frequency1 = 0
var scale1 = 0.2

var phase2 = 0
var frequency2 = 1
var scale2 = 0.2

var phase3 = 0
var frequency3 = 2
var scale3 = 0.2

var phaseSpeed = 0.1
var frequencySpeed = 0.1
var scaleSpeed = 0.01
var currentWave = 1
var passData = [0,0,0]

var currentButton = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	shaderMaterial = get_node("SubViewport/display/shader").material


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(Input.is_action_just_released("scrollMinus")):
		if(knob1Active):
			if(currentWave==1):
				phase1-=phaseSpeed
				$knob1.rotate(Vector3(0,1,0),0.05)
				setData(currentWave, [phase1,frequency1,scale1])
			if(currentWave==2):
				phase2-=phaseSpeed
				$knob1.rotate(Vector3(0,1,0),0.05)
				setData(currentWave, [phase2,frequency2,scale2])
			if(currentWave==3):
				phase3-=phaseSpeed
				$knob1.rotate(Vector3(0,1,0),0.05)
				setData(currentWave, [phase3,frequency3,scale3])
		if(knob2Active):
			if(currentWave==1):
				frequency1-=frequencySpeed
				$knob2.rotate(Vector3(0,1,0),0.05)
				setData(currentWave, [phase1,frequency1,scale1])
			if(currentWave==2):
				frequency2-=frequencySpeed
				$knob2.rotate(Vector3(0,1,0),0.05)
				setData(currentWave, [phase2,frequency2,scale2])
			if(currentWave==3):
				frequency3-=frequencySpeed
				$knob2.rotate(Vector3(0,1,0),0.05)
				setData(currentWave, [phase3,frequency3,scale3])
		if(knob3Active):
			if(currentWave==1):
				scale1-=scaleSpeed
				$knob3.rotate(Vector3(0,1,0),0.05)
				setData(currentWave, [phase1,frequency1,scale1])
			if(currentWave==2):
				scale2-=scaleSpeed
				$knob3.rotate(Vector3(0,1,0),0.05)
				setData(currentWave, [phase2,frequency2,scale2])
			if(currentWave==3):
				scale3-=scaleSpeed
				$knob3.rotate(Vector3(0,1,0),0.05)
				setData(currentWave, [phase3,frequency3,scale3])

	
	if(Input.is_action_just_released("scrollPlus")):
		if(knob1Active):
			if(currentWave==1):
				phase1+=phaseSpeed
				$knob1.rotate(Vector3(0,1,0),-0.05)
				setData(currentWave, [phase1,frequency1,scale1])
			if(currentWave==2):
				phase2+=phaseSpeed
				$knob1.rotate(Vector3(0,1,0),-0.05)
				setData(currentWave, [phase2,frequency2,scale2])
			if(currentWave==3):
				phase3+=phaseSpeed
				$knob1.rotate(Vector3(0,1,0),-0.05)
				setData(currentWave, [phase3,frequency3,scale3])
		if(knob2Active):
			if(currentWave==1):
				frequency1+=frequencySpeed
				$knob2.rotate(Vector3(0,1,0),-0.05)
				setData(currentWave, [phase1,frequency1,scale1])
			if(currentWave==2):
				frequency2+=frequencySpeed
				$knob2.rotate(Vector3(0,1,0),-0.05)
				setData(currentWave, [phase2,frequency2,scale2])
			if(currentWave==3):
				frequency3+=frequencySpeed
				$knob2.rotate(Vector3(0,1,0),-0.05)
				setData(currentWave, [phase3,frequency3,scale3])
		if(knob3Active):
			if(currentWave==1):
				scale1+=scaleSpeed
				$knob3.rotate(Vector3(0,1,0),-0.05)
				setData(currentWave, [phase1,frequency1,scale1])
			if(currentWave==2):
				scale2+=scaleSpeed
				$knob3.rotate(Vector3(0,1,0),-0.05)
				setData(currentWave, [phase2,frequency2,scale2])
			if(currentWave==3):
				scale3+=scaleSpeed
				$knob3.rotate(Vector3(0,1,0),-0.05)
				setData(currentWave, [phase3,frequency3,scale3])
	if(Input.is_action_just_pressed("mouseClick")):
		if currentButton==1:
			currentWave=1
			$button1/OmniLight3D.visible=true
			$button2/OmniLight3D.visible=false
			$button3/OmniLight3D.visible=false
		if currentButton==2:
			currentWave=2
			$button1/OmniLight3D.visible=false
			$button2/OmniLight3D.visible=true
			$button3/OmniLight3D.visible=false
		if currentButton==3:
			currentWave=3
			$button1/OmniLight3D.visible=false
			$button2/OmniLight3D.visible=false
			$button3/OmniLight3D.visible=true


func setData(id, data):
	if(id == 1):
		(shaderMaterial as ShaderMaterial).set_shader_parameter("phase1", data[0])
		(shaderMaterial as ShaderMaterial).set_shader_parameter("frequency1", data[1])
		(shaderMaterial as ShaderMaterial).set_shader_parameter("scale1", data[2])
	if(id == 2):
		(shaderMaterial as ShaderMaterial).set_shader_parameter("phase2", data[0])
		(shaderMaterial as ShaderMaterial).set_shader_parameter("frequency2", data[1])
		(shaderMaterial as ShaderMaterial).set_shader_parameter("scale2", data[2])
	if(id == 3):
		(shaderMaterial as ShaderMaterial).set_shader_parameter("phase3", data[0])
		(shaderMaterial as ShaderMaterial).set_shader_parameter("frequency3", data[1])
		(shaderMaterial as ShaderMaterial).set_shader_parameter("scale3", data[2])


func knob1_mouse_entered():
	knob1Active=true


func knob1_mouse_exited():
	knob1Active=false


func knob2_mouse_entered():
	knob2Active=true


func knob2_mouse_exited():
	knob2Active=false


func knob3_mouse_entered():
	knob3Active=true


func knob3_mouse_exited():
	knob3Active=false


func button1_mouse_entered():
	currentButton = 1

func button2_mouse_entered():
	currentButton = 2

func button3_mouse_entered():
	currentButton = 3

func button_mouse_exited():
	currentButton = 0
