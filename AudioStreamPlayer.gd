extends AudioStreamPlayer

var device 
#

@onready var _playback := get_stream_playback()
@onready var _sample_hz:float = stream.mix_rate
var _pulse_hz := 440.0
var _phase1 := 0.0
var _phase2 := 0.0
var _phase3 := 0.0


func _ready():
	device = get_parent()
	_fill_buffer()


func _process(_delta):
	_fill_buffer()
	if(!playing):
		play()


func _fill_buffer():
	var increment1 := (_pulse_hz*float(device.frequency1)/3.0) / _sample_hz
	var increment2 := (_pulse_hz*float(device.frequency2)/3.0) / _sample_hz
	var increment3 := (_pulse_hz*float(device.frequency3)/3.0) / _sample_hz
	for frame_index in int(_playback.get_frames_available()):
		_playback.push_frame(Vector2.ONE * (sin(_phase1  * TAU + device.phase1* TAU) * device.scale1 + sin(_phase2  * TAU + device.phase2* TAU) * device.scale2 + sin(_phase3  * TAU + device.phase3* TAU) * device.scale3  ))
		_phase1 = fmod(_phase1 + increment1, 1.0)
		_phase2 = fmod(_phase2 + increment2, 1.0)
		_phase3 = fmod(_phase3 + increment3, 1.0)
